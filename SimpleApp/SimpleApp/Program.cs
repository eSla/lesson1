﻿using System;

namespace SimpleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Simple application! Please, write your name.");
            string name = Console.ReadLine();
            Console.WriteLine($"{name}, hello. Nice to meet you.");
        }
    }
}
